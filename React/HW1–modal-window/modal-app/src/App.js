import React, { useState } from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";
import "./App.scss";

const App = () => {
  const [isFirstModalOpen, setFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setSecondModalOpen] = useState(false);

  const openFirstModal = () => setFirstModalOpen(true);
  const openSecondModal = () => setSecondModalOpen(true);
  const closeFirstModal = () => setFirstModalOpen(false);
  const closeSecondModal = () => setSecondModalOpen(false);

  return (
    <div className="app">
      <Button
        backgroundColor="blue"
        text="Open first modal"
        onClick={openFirstModal}
      />
      <Button
        backgroundColor="green"
        text="Open second modal"
        onClick={openSecondModal}
      />

      {isFirstModalOpen && (
        <Modal
          header="Do you want to delete this file?"
          closeButton={true}
          text="Once you delete this file, it won`t be possible to undo this action."
          text2="Are you sure you want to delete it?"
          actions={[
            <Button
              key="closeButton"
              backgroundColor="#A02C12"
              text="Ok"
              onClick={closeFirstModal}
            />,
            <Button
              key="closeButton"
              backgroundColor="#A02C12"
              text="Cancel"
              onClick={closeFirstModal}
            />
          ]}
          onClose={closeFirstModal}
        />
      )}

      {isSecondModalOpen && (
        <Modal
          header="Danger warning"
          closeButton={true}
          text="Your code has been detected with a malicious virus."
          text2="We recommend eliminating the hazard."
          actions={[
            <Button
              key="closeButton"
              backgroundColor="red"
              text="Eliminate"
              onClick={closeSecondModal}
            />,
            <Button
              key="closeButton"
              backgroundColor="red"
              text="I like the virus"
              onClick={closeSecondModal}
            />
          ]}
          onClose={closeSecondModal}
        />
      )}
    </div>
  );
};

export default App;
