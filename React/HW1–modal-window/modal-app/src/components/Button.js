import React from "react";

const Button = ({ backgroundColor, text, onClick }) => {
  const buttonStyle = {
    backgroundColor,
    padding: "10px 20px",
    borderRadius: "5px",
    color: "#fff",
    cursor: "pointer",
  };

  return (
    <button style={buttonStyle} onClick={onClick}>
      {text}
    </button>
  );
};

export default Button;
