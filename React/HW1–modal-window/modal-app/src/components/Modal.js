import React from 'react';
import './Modal.scss';

const Modal = ({ header, closeButton, text, text2, actions, onClose }) => {
  const handleOverlayClick = (event) => {
    if (event.target === event.currentTarget) {
      onClose();
    }
  };

  return (
    <div className="modal-overlay" onClick={handleOverlayClick}>
      <div className="modal">
        <div className="modal-header">
          <span>{header}</span>
          {closeButton && (
            <span className="modal-close" onClick={onClose}>
              &times;
            </span>
          )}
        </div>
        <div className="modal-content">{text}</div>
        <div className="modal-content">{text2}</div>
        <div className="modal-actions">
          {actions.map((action, index) => (
            <React.Fragment key={index}>{action}</React.Fragment>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Modal;
