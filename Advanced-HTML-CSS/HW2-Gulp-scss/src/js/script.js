const navIcon = document.querySelector(".navigation__icon");
const navUl = document.querySelector(".navigation__list");
const navlink = document.querySelectorAll(".navigation__link");
const navbtn = document.querySelector(".navigation__icon");
const postText = document.querySelectorAll(".recent-posts-text");
const instSecTitle = document.querySelector(".instagram-second-title");
const instHiddenImg = document.querySelectorAll(".hidden-image");
const footerHidden = document.querySelector(".footer_hidden-description");
const instImg = document.querySelectorAll(".instagram-image");
//instagram-image

navIcon.addEventListener("click", function () {
  if (navIcon.innerText == "menu") {
    navIcon.innerText = "close";
  } else {
    navIcon.innerText = "menu";
  }
  navUl.classList.toggle("invisible");
});

navUl.addEventListener("click", function () {
  navlink.forEach((elem) => {
    elem.style.color = "#4E4E4E";
  });
  event.target.style.color = "#57A4A4";
});

if (window.matchMedia("(min-width: 768px)").matches) {
  navUl.classList.remove("invisible");
  postText.forEach((elem) => {
    elem.classList.remove("invisible");
  });
  instSecTitle.classList.remove("invisible");
  instHiddenImg.forEach((elem) => {
    elem.classList.remove("invisible");
  });
  footerHidden.classList.remove("invisible");
}

if (window.matchMedia("(min-width: 1200px)").matches) {
  instImg.forEach((elem) => {
    elem.classList.remove("invisible");
  });
}
