const gulp = require("gulp");
const gulpClean = require("gulp-clean");
const cssmin = require("gulp-cssmin");
const rename = require("gulp-rename");
const sass = require("gulp-sass")(require("sass"));
const concat = require("gulp-concat");
const imagemin = require("gulp-imagemin");
const minifyjs = require("gulp-js-minify");
const autoprefixer = require("gulp-autoprefixer");

const { series } = require("gulp");

const addPrefix = function () {
  return gulp
    .src("dist/css/*")
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(gulp.dest("dist/css/"));
};

const generateImage = function () {
  return gulp.src("src/img/*").pipe(imagemin()).pipe(gulp.dest("dist/img/"));
};

const cleanDist = function () {
  return gulp.src("dist/*").pipe(gulpClean());
};

const generateMinCss = function () {
  return gulp
    .src("src/scss/*.scss")
    .pipe(sass({ outputStyle: "expanded" }).on("error", sass.logError))
    .pipe(concat("main.css"))
    .pipe(cssmin())
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("dist/css/"));
};

const generateMinJs = () => {
  return gulp
    .src("src/js/*.js")
    .pipe(concat("all.js"))
    .pipe(minifyjs())
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("./dist/js/"));
};

function build() {
  return gulp.series(
    cleanDist,
    gulp.parallel(generateMinCss, generateMinJs, generateImage),
    addPrefix
  );
}

function watch() {
  gulp.watch("src/scss/*.scss", build());
}

//gulp.task("watch", watch);
exports.watch = watch;

gulp.task("cleanDist", cleanDist);
gulp.task("generateMinCss", generateMinCss);
gulp.task("generateMinJs", generateMinJs);
gulp.task("generateImage", generateImage);
gulp.task("addPrefix", addPrefix);

gulp.task(
  "build",
  gulp.series(
    cleanDist,
    gulp.parallel(generateMinCss, generateMinJs, generateImage),
    addPrefix
  )
);

exports.default = build;

//gulp watch
