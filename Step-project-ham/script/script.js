const ulElement = document.querySelector(".our-services-list");

const liElement = document.querySelectorAll(".our-services-list-item");

ulElement.addEventListener("click", function (event) {
  liElement.forEach(function (element) {
    if (element.classList.contains("active")) {
      element.classList.remove("active");
    }
  });
  if (!event.target.classList.contains("active")) {
    event.target.classList.add("active");
  }

  for (const key of document.querySelectorAll(".our-services-content-item")) {
    let targetAttr = event.target.dataset.tab;
    let elem = key.dataset.content;
    if (!key.classList.contains("invisible")) {
      key.classList.add("invisible");
    }
    if (elem === targetAttr) {
      key.classList.remove("invisible");
    }
  }
});

const filterTabs = document.querySelectorAll(".third-page-filter-item");
const cards = document.querySelectorAll(".card");
const ulFilter = document.querySelector(".third-page-filter");

ulFilter.addEventListener("click", function (event) {
  filterTabs.forEach((elem) => {
    elem.classList.remove("active-tab");
  });
  event.target.classList.add("active-tab");
  let activeTab = event.target.dataset.filter;
  cards.forEach((e) => {
    e.style.display = "none";
    let cardCategory = e.dataset.filter;
    let hiddenattr = e.getAttribute("data-hidden");
    if (activeTab === "All" && hiddenattr === null) {
      e.style.display = "block";
    } else if (activeTab === cardCategory && hiddenattr === null) {
      e.style.display = "block";
    }
  });
  if (hiddenattr.dataset.filter == activeTab) {
    for (const i of hiddenattr) {
      i.classList.remove("invisible");
    }
  }
});

//
const loadMore = document.querySelector(".third-page-wrap > button");
const invisibleImg = document.querySelectorAll("span.invisible");

//console.log(invisibleImg);

loadMore.addEventListener("click", function () {
  for (const key of invisibleImg) {
    key.classList.remove("invisible");
  }
  cards.forEach((e) => {
    e.removeAttribute("data-hidden");
  });
  filterTabs.forEach((el) => {
    if (el.classList.contains("active-tab")) {
      let activeTabAttr = el.getAttribute("data-filter");
      cards.forEach((element) => {
        let cardAttr = element.getAttribute("data-filter");
        if (activeTabAttr === cardAttr) {
          element.classList.remove("invisible");
        } else if (activeTabAttr === "All") {
          element.classList.remove("invisible");
        }
      });
    }
  });
  loadMore.style.display = "none";
});

// карусель

let imgArr = document.querySelectorAll(".feedback-slider__photo");

let srcArr = [];
let className = [];
let dataSet = [];

for (let i = 0; i < imgArr.length; i++) {
  srcArr[i] = imgArr[i].src;
  className[i] = imgArr[i].getAttribute("class");
  dataSet[i] = imgArr[i].getAttribute("data-id");
  imgArr[i].remove();
}

let step = 0;
// let offset = 0;

function addImg() {
  let img = document.createElement("img");
  img.src = srcArr[step];
  img.setAttribute("class", className[step]);
  img.setAttribute("data-id", dataSet[step]);
  document.querySelector(".feedback-slider__photo-wrap").appendChild(img);

  if (step === srcArr.length - 1) {
    step = 0;
  } else {
    step++;
  }
}

addImg();
addImg();
addImg();
addImg();

function next() {
  let imgArray = document.querySelectorAll(".feedback-slider__photo");
  let offset2 = 0;
  for (let i = 0; i < imgArray.length; i++) {
    imgArray[i].style.left = offset2 * 80 - 80 + "px";
    offset2++;
  }
  setTimeout(function () {
    className[step] = imgArray[0].getAttribute("class");
    imgArray[0].remove();
    addImg();
  }, 500);
}

function addImgPrep() {
  let img = document.createElement("img");
  img.src = srcArr[step - 1];
  img.setAttribute("class", className[step - 1]);
  img.setAttribute("data-id", dataSet[step - 1]);
  let childFirst = document.querySelector(
    ".feedback-slider__photo-wrap"
  ).firstChild;
  document
    .querySelector(".feedback-slider__photo-wrap")
    .insertBefore(img, childFirst);
  step--;
}

function prev() {
  let imgArray = document.querySelectorAll(".feedback-slider__photo");
  let offset2 = 0;
  for (let i = 0; i < imgArray.length; i++) {
    imgArray[i].style.left = offset2 * 80 - 80 + "px";
    offset2++;
  }
  setTimeout(function () {
    if (step === 0) {
      step = srcArr.length;
    }
    className[step - 1] = imgArray[srcArr.length - 1].getAttribute("class");
    imgArray[srcArr.length - 1].remove();
    addImgPrep();
  }, 500);
}

function showFeedback(event) {
  const sliderPhotos = document.querySelectorAll(".feedback-slider__photo");
  const feedbackText = document.querySelectorAll(".feedback-text__item");
  let currentPhoto = event.target;
  let titleData = currentPhoto.getAttribute("data-id");
  let currentFeedbackText = document.querySelector(titleData);
  if (!currentPhoto.classList.contains("active")) {
    sliderPhotos.forEach((el) => {
      el.classList.remove("active");
    });
    feedbackText.forEach((element) => {
      element.classList.remove("active");
    });
  }
  currentPhoto.classList.add("active");
  currentFeedbackText
    .querySelector(".feedback-text__people-photo")
    .setAttribute("src", currentPhoto.getAttribute("src"));
  currentFeedbackText.classList.add("active");
}

function getIndex() {
  let sliderPhotosArray = document.querySelectorAll(".feedback-slider__photo");
  let ind;
  for (let i = 0; i < sliderPhotosArray.length; i++) {
    if (sliderPhotosArray[i].classList.contains("active")) {
      if (i === 0) {
        ind = sliderPhotosArray.length;
      }
      ind = i;
    }
  }
  return ind;
}

function showFeedback2(elem) {
  const sliderPhotos = document.querySelectorAll(".feedback-slider__photo");
  const feedbackText = document.querySelectorAll(".feedback-text__item");
  let currentPhoto = elem;
  let dataTitle = currentPhoto.getAttribute("data-id");
  let currentFeedbackText = document.querySelector(dataTitle);
  if (!currentPhoto.classList.contains("active")) {
    sliderPhotos.forEach((el) => {
      el.classList.remove("active");
    });
    feedbackText.forEach((element) => {
      element.classList.remove("active");
    });
  }
  currentPhoto.classList.add("active");
  currentFeedbackText
    .querySelector(".feedback-text__people-photo")
    .setAttribute("src", currentPhoto.getAttribute("src"));
  currentFeedbackText.classList.add("active");
}

document.querySelector(".feedback-slider").addEventListener("click", (e) => {
  if (e.target.closest(".btn-slider__next")) {
    let currentIndex = getIndex();
    showFeedback2(
      document.querySelectorAll(".feedback-slider__photo")[currentIndex + 1]
    );
    next();
  }
  if (e.target.closest(".btn-slider__prev")) {
    let currentIndex = getIndex();
    if (currentIndex === 0) {
      currentIndex = document.querySelectorAll(
        ".feedback-slider__photo"
      ).length;

      showFeedback2(
        document.querySelectorAll(".feedback-slider__photo")[currentIndex - 1]
      );
      prev();
    } else {
      showFeedback2(
        document.querySelectorAll(".feedback-slider__photo")[currentIndex - 1]
      );
      prev();
    }
  }
  if (e.target.closest(".feedback-slider__photo")) {
    showFeedback(e);
  }
});

// gallery masonry

window.onload = function () {
  const elem = document.querySelector(".grid");
  const msnry = new Masonry(elem, {
    // options
    itemSelector: ".grid-item",
    columnWidth: 386,
  });
};

const gridItem = document.querySelectorAll(".grid-item");

function getImg() {
  gridItem.forEach((el) => {
    if (!el.classList.contains("active")) {
      el.classList.add("active");
    }
  });
  const elem = document.querySelector(".grid");
  const msnry = new Masonry(elem, {
    // options
    itemSelector: ".grid-item",
    columnWidth: 386,
  });
  document.querySelector(".gallery-section__btn").remove();
}

function zoomImg(e) {
  let element = e.target;
  let img = element.parentElement.querySelector("img");
  img.style.transform = "scale(1.2)";
  element.parentElement.style.zIndex = "1";
  img.style.background = "rgba(0, 0, 0, 0)";
  element.style.display = "none";
  img.style.opacity = "100%";
  setTimeout(function () {
    img.style.transform = "";
    element.parentElement.style.zIndex = "";
    img.style.background = "";
    img.style.opacity = "";
    element.style.display = "";
  }, 3000);
}

const modal = document.querySelector(".modal");
const modalImg = document.querySelector(".modal-img");

document
  .querySelector(".gallery-section")
  .addEventListener("click", (event) => {
    if (event.target.closest(".gallery-section__btn")) {
      document.querySelector(".gallery-section__btn img").style.display =
        "none";
      document.querySelector(".gallery-section__btn p").style.display = "none";
      document.querySelector(".gallery-section__btn h2").style.display = "flex";
      setTimeout(getImg, 2000);
    }
    if (event.target.classList.contains("gallery-zoom-btn")) {
      zoomImg(event);
    }
    if (event.target.classList.contains("gallery-full-screen-btn")) {
      const imgSrc = event.target.parentElement
        .querySelector("img")
        .getAttribute("src");
      const imgAlt = event.target.parentElement
        .querySelector("img")
        .getAttribute("alt");
      modalImg.setAttribute("src", imgSrc);
      modalImg.setAttribute("alt", imgAlt);
      modal.style.display = "block";
      modal.addEventListener("click", () => {
        modal.style.display = "none";
      });
    }
  });
