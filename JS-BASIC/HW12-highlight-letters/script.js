document.addEventListener("keydown", function (event) {
  for (const key of document.querySelectorAll(".btn")) {
    let elem = key.dataset.btn;

    if (key.classList.contains("blue")) {
      key.classList.remove("blue");
    }
    if (elem === event.code) {
      key.classList.add("blue");
    }
  }
});
