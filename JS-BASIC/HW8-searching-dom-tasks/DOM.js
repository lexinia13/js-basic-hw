// #1

const pElement = document.querySelectorAll("p");
for (const key of pElement) {
  key.style.backgroundColor = "#ff0000";
}

// #2
let optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);

optionsList.hasChildNodes = () => {
  if (true) {
    for (let i = 0; i < optionsList.childNodes.length; i++) {
      console.log(`Название: ${optionsList.childNodes[i].nodeName}`);
      console.log(`Тип нод-ребенка: ${typeof optionsList.childNodes}`);
    }
  }
};
optionsList.hasChildNodes();

// #3

let testParagraph = document.getElementById("testParagraph");
testParagraph.textContent = "This is a paragraph";

// #4

const mainHeader = document.querySelector(".main-header");
const children = mainHeader.children;
console.log(mainHeader);
console.log(children);

for (const key of children) {
  for (let index = 0; index <= 0; index++) {
    key.classList.add("nav-item");
  }
}
//console.log(children);

// #5

const sectionTitle = document.querySelectorAll(".section-title");

for (const key of sectionTitle) {
  for (let index = 0; index <= 0; index++) {
    key.classList.remove("section-title");
  }
}

console.log(sectionTitle);
