function getNumber() {
  let number = Number(prompt("Введите число"));
  while (Number.isNaN(number)) {
    number = Number(prompt("Введите число"));
  }
  return number;
}

function getOperation() {
  let oper = prompt("Введите операцию");
  while (oper !== "+" && oper !== "-" && oper !== "/" && oper !== "*") {
    oper = prompt("Введите операцию", oper);
  }
  return oper;
}

function executeOperation() {

  const symbol = getOperation();

  switch (symbol) {
    case "+":
      return firstNumber + secondNumber;
    case "-":
      return firstNumber - secondNumber;
    case "/":
      return firstNumber / secondNumber;
    case "*":
      return firstNumber * secondNumber;
  }
}

console.log(executeOperation());
