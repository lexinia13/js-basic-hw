let items = ["hello", "world", 23, "23", null];

const filterBy = (arr, type) => arr.filter((item) => typeof item !== type);

console.log(filterBy(items, "string"));

/*
let result = arr.filter(function(array, data) {
    // якщо true - елемент додається до результату, і перебір триває
    // повертається порожній масив в разі, якщо нічого не знайдено
  });
  */
