document
  .querySelector(".password-form")
  .addEventListener("click", function (event) {
    if (event.target.tagName == "I") {
      const input = document.querySelector(`.${event.target.dataset.icon}`);
      if (event.target.classList.contains("fa-eye")) {
        event.target.classList.replace("fa-eye", "fa-eye-slash");
        event.target.style.display = "block";
        input.type = "text";
      } else {
        event.target.classList.replace("fa-eye-slash", "fa-eye");
        input.type = "password";
      }
    }
  });

document
  .querySelector(".password-form")
  .addEventListener("submit", function (event) {
    event.preventDefault();
    const error = document.getElementById("message");
    if (
      document.querySelector(".firstInput").value ===
      document.querySelector(".secoundInput").value
    ) {
      alert("You are welcome!");
      error.style.display = "none";
    } else {
      error.style.display = "block";
    }
  });
