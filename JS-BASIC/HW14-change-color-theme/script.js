// href="./dark.css"
//href="./light.css"
// localStorage.clear()

const btn = document.getElementById("btn");
const theme = document.querySelector("#theme-link");
const dark = "./dark.css";
const light = "./light.css";

btn.addEventListener("click", function () {
  if (theme.getAttribute("href") == dark) {
    theme.setAttribute("href", light);
    localStorage.setItem("theme", light);
  } else {
    theme.setAttribute("href", dark);
    localStorage.setItem("theme", dark);
  }
});

document.addEventListener("DOMContentLoaded", function () {
  if (localStorage.getItem("theme") == light) {
    theme.setAttribute("href", light);
  } else {
    theme.setAttribute("href", dark);
  }
  console.log(localStorage.getItem("theme"));
});
