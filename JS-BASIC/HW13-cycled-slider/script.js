const buttons = document.querySelector("#buttons");
const resumeBtn = document.querySelector("#resume");
const stopBtn = document.querySelector("#stop");
const img = document.querySelector("img");

const arrImg = ["1.jpg", "2.jpg", "3.jpg", "4.jpg"];

let slide = 0;

function showPic() {
  img.src = "./img/" + arrImg[slide];
  slide++;
  if (slide == arrImg.length) {
    slide = 0;
  }
}

let idImg = setInterval(showPic, 500);

resumeBtn.addEventListener("click", function () {
  idImg = setInterval(showPic, 500);
});

stopBtn.addEventListener("click", function () {
  clearInterval(idImg);
});
