const name = prompt("What is your first name?");
const surname = prompt("What is your last name?");

const firstUser = (newUser = {});
const date = new Date(); //Текущая дата

const birthday = prompt("What is your date of birth?", "dd.mm.yyyy").split(".");
new Date(birthday[2], birthday[1], birthday[0]);

const thisYear = new Date(
  date.getFullYear(),
  birthday[1] - 1, //месяц, с 0
  birthday[0] //день
); //ДР в текущем году

const createNewUser = (firstName = null, lastName = null) => {
  newUser = {
    firstName,
    lastName,
    getLogin() {
      return firstName.toLowerCase()[0] + lastName.toLowerCase();
    },

    getAge() {
      let age = date.getFullYear() - birthday[2];
      if (date < thisYear) {
        age--;
      }
      return age;
    },

    getPassword() {
      return firstName.toUpperCase()[0] + lastName.toLowerCase() + birthday[2];
    },
  };
  return newUser;
};

const user = createNewUser(name, surname);

console.log(`Логин: ${user.getLogin()}`);
console.log(`Возраст: ${user.getAge()}`);
console.log(`Пароль: ${user.getPassword()}`);


// alert(`Завантаження розпочалося ${performance.now()}мс назад`);
