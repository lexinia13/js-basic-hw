const ulElement = document.querySelector(".tabs");

const liElement = document.querySelectorAll(".tabs-title");

ulElement.addEventListener("click", function (event) {
  liElement.forEach(function (element) {
    if (element.classList.contains("active")) {
      element.classList.remove("active");
    }
  });
  if (!event.target.classList.contains("active")) {
    event.target.classList.add("active");
  }

  for (const key of document.querySelectorAll(".tabs-content-text")) {
    let targetAttr = event.target.dataset.tab;
    let elem = key.dataset.content;
    if (!key.classList.contains("invisible")) {
      key.classList.add("invisible");
    }
    if (elem === targetAttr) {
      key.classList.remove("invisible");
    }
  }
});