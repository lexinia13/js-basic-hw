//ipstack.com или geolocation-db.com

//ip-api.com/ не дает бесплатно им пользоваться, 
//поэтому я использовала ipstack.com

async function fetchData(url) {
  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error(`Ошибка HTTP: ${response.status}`);
    }

    return await response.json();
  } catch (error) {
    throw new Error(`Не удалось загрузить данные с ${url}: ${error}`);
  }
}

document.getElementById("findIpButton").addEventListener("click", async () => {
  try {
    const ipResponse = await fetch("https://api.ipify.org/?format=json");
    const { ip } = await ipResponse.json();

    const access_key = "8effff76ec8e2d3324d08713293c73f8";
    const locationUrl = `http://api.ipstack.com/${ip}?access_key=${access_key}`;
    const locationData = await fetchData(locationUrl);

    displayLocationData(locationData);
  } catch (error) {
    console.log("Произошла ошибка:", error);
  }
});

function displayLocationData(data) {
  const resultDiv = document.getElementById("result");
  resultDiv.innerHTML = `
      <p>Континент: ${data.continent_name}</p>
      <p>Страна: ${data.country_name}</p>
      <p>Регион: ${data.region_name}</p>
      <p>Город: ${data.city}</p>
      <p>Район: ${data.district}</p>
    `;
}
