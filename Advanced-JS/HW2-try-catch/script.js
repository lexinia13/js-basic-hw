const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

function validateBook(book) {
  if (!book.hasOwnProperty("author")) {
    throw new Error(`В об'єкті відсутня властивість 'author'`);
  }
  if (!book.hasOwnProperty("name")) {
    throw new Error(`В об'єкті відсутня властивість 'name'`);
  }
  if (!book.hasOwnProperty("price")) {
    throw new Error(`В об'єкті відсутня властивість 'price'`);
  }
}

function renderBooks(books) {
  const list = document.createElement("ul");

  for (let i = 0; i < books.length; i++) {
    try {
      validateBook(books[i]);

      const listItem = document.createElement("li");
      listItem.textContent = `Автор: ${books[i].author}, Назва: ${books[i].name}, Ціна: ${books[i].price}`;

      list.appendChild(listItem);
    } catch (error) {
      console.error(error.message);
    }
  }

  document.querySelector("#root").appendChild(list);
}

renderBooks(books);
