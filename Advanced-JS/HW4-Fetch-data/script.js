fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error("Ошибка при получении списка фильмов");
    }
  })
  .then((films) => {
    films.sort((a, b) => a.episodeId - b.episodeId);
    const filmsListElement = document.getElementById("films-list");
    films.forEach((film) => {
      const filmElement = document.createElement("div");
      filmElement.innerHTML = `
        <h2>Episode ${film.episodeId}: ${film.name}</h2>
        <p>${film.openingCrawl}</p>
        <h3>Characters:</h3>
        <ul id="characters-${film.episodeId}"></ul>`;
      filmsListElement.appendChild(filmElement);
      film.characters.forEach((characterURL) => {
        fetch(characterURL)
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              throw new Error(
                `Ошибка при получении информации о персонаже: ${characterURL}`
              );
            }
          })
          .then((character) => {
            const characterListElement = document.getElementById(
              `characters-${film.episodeId}`
            );
            const characterItemElement = document.createElement("li");
            characterItemElement.textContent = character.name;
            characterListElement.appendChild(characterItemElement);
          })
          .catch((error) => console.error(error));
      });
    });
  })
  .catch((error) => console.error(error));
