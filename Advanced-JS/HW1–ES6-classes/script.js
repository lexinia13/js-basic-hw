class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}
/*
const employee = new Employee("Alex", 27, "500 USD");
console.log(
  "Name: " + employee.name,
  "Age: " + employee.age,
  "Salary: " + employee.salary
);

const employee2 = new Employee("Mari", 18, "200 USD");
console.log(
  "Name: " + employee2.name,
  "Age: " + employee2.age,
  "Salary: " + employee2.salary
);
*/

class Programmer extends Employee {
  lang = "english, ukrainian, french";

  constructor(name, age, salary) {
    super(name, age, salary);
  }

  get salary() {
    return this._salary * 3;
  }
  set salary(value) {
    this._salary = value;
  }
}

const programmer = new Programmer("Alex", 27, 500);
console.log(
  "Name: " + programmer.name + ",",
  "Age: " + programmer.age + ",",
  "Salary: " + programmer.salary
);

const programmer2 = new Programmer("Mari", 18, 200);
console.log(
  "Name: " + programmer2.name + ",",
  "Age: " + programmer2.age + ",",
  "Salary: " + programmer2.salary
);

const programmer3 = new Programmer("Max", 34, 800);
console.log(
  "Name: " + programmer3.name + ",",
  "Age: " + programmer3.age + ",",
  "Salary: " + programmer3.salary
);
