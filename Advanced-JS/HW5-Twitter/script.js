class Card {
  constructor(user, post) {
    this.user = user;
    this.post = post;
  }

  createCardElement() {
    const cardElement = document.createElement("div");
    cardElement.classList.add("card");

    const titleElement = document.createElement("h3");
    titleElement.textContent = this.post.title;

    const textElement = document.createElement("p");
    textElement.textContent = this.post.text;

    const userElement = document.createElement("p");
    userElement.textContent = `${this.user.name} (${this.user.email})`;

    const deleteButton = document.createElement("button");
    deleteButton.textContent = "Удалить";
    deleteButton.addEventListener("click", () => this.deleteCard());

    const editButton = document.createElement("button");
    editButton.textContent = "Редактировать";
    editButton.addEventListener("click", () => this.editCard());

    cardElement.appendChild(titleElement);
    cardElement.appendChild(textElement);
    cardElement.appendChild(userElement);
    cardElement.appendChild(deleteButton);
    cardElement.appendChild(editButton);

    return cardElement;
  }

  deleteCard() {
    // Отправить DELETE запрос на сервер для удаления публикации
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
      method: "DELETE",
    })
      .then((response) => {
        if (response.ok) {
          // Если запрос прошел успешно, удалить карточку со страницы
          const cardElement = document.getElementById(`card-${this.post.id}`);
          cardElement.remove();
        }
      })
      .catch((error) => {
        console.error("Ошибка удаления публикации:", error);
      });
  }
  editCard() {
    const newTitle = prompt("Введите новый заголовок", this.post.title);
    const newText = prompt("Введите новый текст", this.post.text);

    if (newTitle && newText) {
      this.post.title = newTitle;
      this.post.text = newText;

      const titleElement = document.getElementById(`title-${this.post.id}`);
      const textElement = document.getElementById(`text-${this.post.id}`);

      titleElement.textContent = newTitle;
      textElement.textContent = newText;
      // Отправить PUT-запрос на сервер для сохранения изменений
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(this.post),
      })
        .then((response) => {
          if (!response.ok) {
            console.error("Ошибка при сохранении изменений");
          }
        })
        .catch((error) => {
          console.error("Ошибка при сохранении изменений:", error);
        });
      ///здесь конец запроса
    }
  }
}

document.getElementById("add-post-button").addEventListener("click", () => {
  document.getElementById("modal-overlay").style.display = "flex";
});

document.getElementById("add-post").addEventListener("click", () => {
  const title = document.getElementById("post-title").value;
  const text = document.getElementById("post-text").value;

  if (title && text) {
    const newPost = {
      title: title,
      text: text,
      userId: 1, // ID пользователя, которому принадлежит публикация
    };

    fetch("https://ajax.test-danit.com/api/json/posts", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newPost),
    })
      .then((response) => response.json())
      .then((data) => {
        // Создать карточку с новой публикацией и добавить ее в начало списка
        const user = {
          id: 1,
          name: "Имя пользователя",
          email: "user@example.com",
        };
        const card = new Card(user, data);
        const cardElement = card.createCardElement();
        cardElement.id = `card-${data.id}`;

        const newsFeedElement = document.getElementById("news-feed");
        newsFeedElement.insertBefore(cardElement, newsFeedElement.firstChild);
      })
      .catch((error) => {
        console.error("Ошибка при добавлении публикации:", error);
      });
    // Скрыть модальное окно и очистить поля ввода
    document.getElementById("modal-overlay").style.display = "none";
    document.getElementById("post-title").value = "";
    document.getElementById("post-text").value = "";
  }
});

// Отправляем GET запрос на получение списка пользователей
fetch("https://ajax.test-danit.com/api/json/users")
  .then((response) => response.json())
  .then((users) => {
    // Отправляем GET запрос на получение списка публикаций
    fetch("https://ajax.test-danit.com/api/json/posts")
      .then((response) => response.json())
      .then((posts) => {
        // Создаем карточку для каждой публикации и добавляем их на страницу
        const newsFeedElement = document.getElementById("news-feed");
        newsFeedElement.style.display = "block";
        const loading = document.getElementById("loading");
        loading.style.display = "none";
        posts.forEach((post) => {
          const user = users.find((user) => user.id === post.userId);
          const card = new Card(user, post);
          const cardElement = card.createCardElement();
          cardElement.id = `card-${post.id}`;

          const titleElement = document.createElement("h3");
          titleElement.id = `title-${post.id}`;
          titleElement.textContent = post.title;

          const textElement = document.createElement("p");
          textElement.id = `text-${post.id}`;
          textElement.textContent = post.text;

          cardElement.appendChild(titleElement);
          cardElement.appendChild(textElement);

          newsFeedElement.appendChild(cardElement);
        });
      })
      .catch((error) => {
        console.error("Ошибка загрузки публикаций:", error);
      });
  })
  .catch((error) => {
    console.error("Ошибка загрузки пользователей:", error);
  });
